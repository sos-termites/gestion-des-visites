/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useRef, useState} from "react"
import RadioSlackItem from "./RadioSlackItem"


export default function RadioSlack({options, select, selected}) {
	const [isSelected, setIsSelected] = useState(selected)

	/**
	 * Reference which points to the ul (used to focus li children)
	 * @type {React.MutableRefObject<HTMLUListElement>}
	 */
	const ul = useRef()
	return (
		<>
			<div className="pt-4 flex justify-center w-full">
				<div className="w-full max-w-3xl mx-auto">
					<fieldset>
						<ul className="space-y-4" ref={ul}>
							{options.map((element, index) =>
								<RadioSlackItem index={index} key={element.id}
								                active={element.id === isSelected}
								                borderColor={element.borderColor}
								                onClick={() => {
									                setIsSelected(element.id)
									                select(element.id)
									                ul.current.children[index].focus()
								                }}>
									{element.value}
								</RadioSlackItem>)}
						</ul>
					</fieldset>
				</div>
			</div>
		</>
	)
}
