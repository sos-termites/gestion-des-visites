/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import VisitCheckListItem from "./VisitCheckListItem"
import NextVisitScheduling from "./NextVisitScheduling"

export default function VisitCheckList({worksite}) {
	const nextVisit = worksite.scheduled_at ? worksite : worksite.nextScheduledVisits[0]
	const type = nextVisit ? nextVisit.type : worksite.next_visit_type
	return (
		<div className={"flex flex-col mt-2"}>
			<NextVisitScheduling nextVisit={nextVisit}/>
			<div
				className="px-4 py-1 mt-5 text-base rounded-full text-indigo-500 border border-indigo-500 undefined text-center max-w-max">
				{type.long_name}
			</div>
			<ul className={"mt-4"}>
				{type.steps?.map(step =>
					<VisitCheckListItem step_name={step.long_name} step_duration={step.duration} step_done={step.done}
					                    key={step.short_name}/>
				)}
			</ul>
		</div>

	)
}

