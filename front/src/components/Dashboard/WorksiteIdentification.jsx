/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from 'react'
import TextBadge from "../TextBadge"
import {getFormattedPhoneNumber} from "../../utils/functions"

const statusCorrespondance = {
	"curatif": ["red", "Curatif"],
	"curatif_exterieur_uniquement": ["yellow", "Curatif extérieur"],
	"surveillance": ["green", "Surveillance"]
}

export default function WorksiteIdentification({worksite}) {
	const [color, label] = statusCorrespondance[worksite.status.short_name]
	return (
		<div
			className="shadow-lg rounded-xl w-full p-8 bg-indigo-500 dark:bg-gray-800 relative overflow-hidden flex-col">
			<TextBadge color={color}>{label}</TextBadge>
			<div className="text-white text-lg mt-4 mb-2">
				{worksite.name}
			</div>
			<div className="text-white text-sm">
				{worksite.address.street}
			</div>
			<div className="text-white text-sm">
				{worksite.address.postal_code + " " + worksite.address.city}
			</div>
			{worksite.contacts.map((contact, index) =>
				<div className="font-medium text-white mt-4 flex justify-center" key={index.toString()}>
					<a className={"mr-1"}
					   href={`tel:${contact.phone_number}`}>{getFormattedPhoneNumber(contact.phone_number)}</a>
					{contact.name && `(${contact.name})`}
				</div>
			)}
		</div>
	)
}
