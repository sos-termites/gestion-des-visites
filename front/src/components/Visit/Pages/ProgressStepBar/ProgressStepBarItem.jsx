/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from 'react'

export default function ProgressStepBarItem({number, title, description, progressionState, onClick, isSelected}) {
	return (
		<li className="relative overflow-hidden lg:flex-1 cursor-pointer lg:flex items-center" onClick={onClick}>
			<div
				className={`border border-gray-200 overflow-hidden lg:flex-grow ${progressionState === 'next' ? 'border-t-0 rounded-b-md' : progressionState === 'completed' ? 'border-b-0 rounded-t-md' : ''} lg:border-0`}>
				<a aria-current="step"
				   className={`${!isSelected ? 'group' : ''}`}>
					<span
						className={`absolute top-0 left-0 w-1 h-full ${!isSelected ? 'bg-transparent group-hover:bg-gray-200' : 'bg-indigo-600'} lg:w-full lg:h-1 lg:bottom-0 lg:top-auto`}
						aria-hidden="true"/>
					<span
						className={`px-6 py-5 flex items-center text-sm font-medium ${progressionState !== "completed" ? 'lg:pl-9' : ''}`}>
                    <span className="flex-shrink-0">
                      <span
	                      className={`w-10 h-10 flex items-center justify-center ${progressionState === 'next' ? 'border-gray-300 border-2' : progressionState === 'progress' ? 'border-indigo-600 border-2' : 'bg-indigo-600'} rounded-full`}>
                        <span
	                        className={`${progressionState === 'completed' ? 'hidden' : ''}  ${progressionState === 'next' ? 'text-gray-500' : 'text-indigo-600'}`}>{number}
                        </span>
	                      <svg className={`${progressionState !== 'completed' ? 'hidden' : ''} w-6 h-6 text-white`}
	                           data-todo-x-description="Heroicon name: solid/check"
	                           xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"
	                           aria-hidden="true">
		                    <path fillRule="evenodd"
		                          d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
		                          clipRule="evenodd"/>
	                    </svg>
                      </span>

                    </span>
                    <span className="mt-0.5 ml-4 min-w-0 flex flex-col">
                      <span
	                      className={`text-xs font-semibold ${progressionState === 'next' ? 'text-gray-500' : progressionState === 'progress' ? 'text-indigo-600' : ''} uppercase tracking-wide`}>{title}</span>
                      <span className="text-sm font-medium text-gray-500">{description}</span>
                    </span>
                  </span>
				</a>
				<div
					className={`hidden absolute top-0 left-0 w-3 inset-0 ${progressionState !== 'completed' ? 'lg:block' : ''}`}
					aria-hidden="true">
					<svg className="h-full w-full text-gray-300" viewBox="0 0 12 82" fill="none"
					     preserveAspectRatio="none">
						<path d="M0.5 0V31L10.5 41L0.5 51V82" stroke="currentcolor"
						      vectorEffect="non-scaling-stroke"/>
					</svg>
				</div>
			</div>
		</li>
	)
}
