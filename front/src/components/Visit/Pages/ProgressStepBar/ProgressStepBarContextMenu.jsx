/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback} from 'react'
import Plus from "../../../../Icons/Plus"
import ContextMenuItemWithIcon from "../../../ContextMenu/ContextMenuItemWithIcon"
import Whistle from "../../../../Icons/Whistle"
import ContextMenu from "../../../ContextMenu/ContextMenu"
import FinishFlag from "../../../../Icons/FinishFlag"
import {useDispatch} from "react-redux"
import {removeEliminationDataType} from "../../Eliminations/EliminationsSlice"
import {unmountStations} from "../../Stations/stationsSlice"
import {addToObservation} from "../../Observations/ObservationsSlice"
import {getPredefinedObservations} from "../../../../services/observations"
import {useFetchAPI} from "../../../../services/authentication"

export default function ProgressStepBarContextMenu({
	                                                   addStep,
	                                                   deleteStep,
	                                                   lastNumber,
	                                                   eliminationBeginIndex,
	                                                   eliminationEndIndex,
	                                                   preEliminationEndIndex,
	                                                   event_id,
	                                                   worksite,
	                                                   token
                                                   }) {
	/**
	 * Callback used to unmount all SB stations
	 * @type {Function}
	 */
	const unmountSB = useCallback(() => {
		dispatch(unmountStations({
			event_id: event_id,
			type: "sb",
			stations: worksite.stations
		}))
	}, [])

	/**
	 * API Function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Callback used to add pre-elimination text to client observations
	 * @type {Function}
	 */
	const addPreEliminationText = useCallback(() => {
		getPredefinedObservations(API, token, "pre_cfe").then(value => {
			dispatch(addToObservation({
				event_id: event_id,
				type: "client",
				value: value?.[0]?.value ?? ""
			}))
		})
	}, [API, token, event_id])

	/**
	 * Callback used to add an EliminationBegin step
	 * @type {Function}
	 */
	const addEliminationBeginCallback = useCallback(() => {
		addStep({
			number: lastNumber + 1,
			title: "Début d'élimination",
			description: "Constat de début d'élimination",
			status: "next",
			isEliminationBegin: true,
			isPreEliminationEnd: false,
			isEliminationEnd: false,
			components: ["EliminationBegin"]
		})
	}, [addStep, lastNumber])

	/**
	 * Callback used to add an EliminationEnd step
	 * @type {Function}
	 */
	const addEliminationEndCallback = useCallback(
		() => {
			unmountSB()
			addStep({
				number: lastNumber + 1,
				title: "Fin d'élimination",
				description: "Constat de fin d'élimination",
				status: "next",
				isEliminationBegin: false,
				isPreEliminationEnd: false,
				isEliminationEnd: true,
				components: ["EliminationEnd"],
			})
		},
		[addStep, lastNumber]
	)

	/**
	 * Callback used to add an preEliminationEnd step
	 * @type {Function}
	 */
	const addPreEliminationEndCallback = useCallback(
		() => {
			unmountSB()
			addPreEliminationText()
			addStep({
				number: lastNumber + 1,
				title: "Pré-élimination",
				description: "Constat de pré-élimination",
				status: "next",
				isEliminationBegin: false,
				isPreEliminationEnd: true,
				isEliminationEnd: false,
				components: ["PreEliminationEnd"]
			})
		},
		[addStep, lastNumber]
	)

	/**
	 * Dispatch function (redux)
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Callback used to delete elimination data
	 * @type {Function}
	 */
	const deleteEliminationData = useCallback(type => {
		dispatch(removeEliminationDataType({
			event_id: event_id,
			type: type
		}))
	}, [])

	return (
		<ContextMenu openingButton={<Plus/>}
		             buttonClassName={"text-white p-2 bg-blue-500 rounded shadow-md focus:ring-offset-gray-100 focus:ring-blue-500 hover:bg-blue-500"}>
			<ContextMenuItemWithIcon
				onClick={eliminationBeginIndex ? () => {
					deleteStep(eliminationBeginIndex)
					deleteEliminationData("eliminationBegin")
				} : addEliminationBeginCallback}
				className={"text-green-600"}
				disabled={eliminationEndIndex || preEliminationEndIndex}
				icon={<Whistle width={20}/>}>
				<div
					className={"font-semibold text-sm normal-case"}>{eliminationBeginIndex ? "Annuler le début d'élimination" : "Début d'élimination"}
				</div>
			</ContextMenuItemWithIcon>

			<ContextMenuItemWithIcon
				onClick={preEliminationEndIndex ? () => {
					deleteStep(preEliminationEndIndex)
					deleteEliminationData("preEliminationEnd")
				} : addPreEliminationEndCallback}
				className={"text-yellow-600"}
				disabled={eliminationBeginIndex || eliminationEndIndex || worksite.status.short_name !== "curatif"}
				icon={<FinishFlag width={25}/>}>
				<div
					className={"font-semibold text-sm normal-case"}>{preEliminationEndIndex ? "Annuler la pré-élimination" : "Pré-élimination"}
				</div>
			</ContextMenuItemWithIcon>

			<ContextMenuItemWithIcon
				onClick={eliminationEndIndex ? () => {
					deleteStep(eliminationEndIndex)
					deleteEliminationData("eliminationEnd")
				} : addEliminationEndCallback}
				className={"text-red-600"}
				disabled={eliminationBeginIndex || preEliminationEndIndex}
				icon={<FinishFlag width={25}/>}>
				<div
					className={"font-semibold text-sm normal-case"}>{eliminationEndIndex ? "Annuler la fin d'élimination" : "Fin d'élimination"}
				</div>
			</ContextMenuItemWithIcon>
		</ContextMenu>
	)
}
