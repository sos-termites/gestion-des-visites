/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback} from 'react'
import Table from "../../Table/Table"
import Td from "../../Table/Td"
import NumberInput from "../../NumberInput"
import {useDispatch} from "react-redux"
import {updateAdjustments} from "./nextVisitSlice"
import {fromMinutes, normalizeTime, sum} from "pomeranian-durations"
import {CharacteristicDurationsTableLineMemo} from "./CharacteristicDurationsTableLine"
import {ISO8601NegativeNormalize} from "../../../utils/functions"

export default function CharacteristicDurationsTable({durations, newAdjustments, event_id}) {

	/**
	 * Callback used to get the original duration of the visit step
	 * @type {Function}
	 */
	const getDuration = useCallback(type => {
		return ISO8601NegativeNormalize(durations.filter(d => d.short_name === type)?.[0]?.duration)
	}, [durations])

	/**
	 * Callback used to get the adjustment duration of the visit step
	 * @type {Function}
	 */
	const getAdjustment = useCallback(type => {
		return ISO8601NegativeNormalize(durations.filter(d => d.short_name === type)?.[0]?.adjustments?.[0]?.duration)
	}, [durations])

	/**
	 * Callback used to get the total duration of the visit step
	 * @type {Function}
	 */
	const getTotalDuration = useCallback(type => {
		return normalizeTime(sum([getDuration(type), getAdjustment(type), fromMinutes(newAdjustments[type])]))
	}, [getAdjustment, getDuration, newAdjustments])

	/**
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Callback used to update adjustments
	 * @type {Function}
	 */
	const onChangeAdjustment = useCallback((type, input) => {
		// noinspection JSCheckFunctionSignatures
		dispatch(updateAdjustments({
			event_id,
			type,
			value: parseInt(input.target.value) || 0
		}))
	}, [])

	return (
		<div className={"max-w-3xl shadow overflow-hidden sm:rounded-lg m-4"}>
			<Table className={"text-center text-gray-600 border-0"} trs={[
				<CharacteristicDurationsTableLineMemo key={"tmps_theo"} label={"Temps théorique"}
				                                      getFunction={getDuration}/>,
				<CharacteristicDurationsTableLineMemo key={"ajustements"} label={"Ajustements préc."}
				                                      getFunction={getAdjustment}/>,
				<tr key={"ajustements_supp"}>
					<Td label={"Type"}>
						<div className={"text-base"}>
							Ajustements supp.
						</div>
					</Td>
					<Td label={"SSOL"}>
						<NumberInput min={-30} max={30} step={5} className={"w-16"} value={newAdjustments["ssol_all"]}
						             onChange={input => onChangeAdjustment("ssol_all", input)}/>
						<span>min</span>
					</Td>
					<Td label={"SB"} className={""}>
						<NumberInput min={-30} max={30} step={5} className={"w-16"} value={newAdjustments["sb"]}
						             onChange={input => onChangeAdjustment("sb", input)}/>
						<span>min</span>
					</Td>
					<Td label={"CB"}>
						<NumberInput min={-30} max={30} step={5} className={"w-16"} value={newAdjustments["cb"]}
						             onChange={input => onChangeAdjustment("cb", input)}/>
						<span>min</span>
					</Td>
				</tr>,
				<CharacteristicDurationsTableLineMemo key={"tmps_ajust"} label={"Temps ajusté"}
				                                      getFunction={getTotalDuration}/>
			]} ths={[" ", "SSOL", "SB", "CB"]}/>
		</div>
	)
}
