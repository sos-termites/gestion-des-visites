/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from 'react'
import Td from "../../Table/Td"
import {getFormattedDuration} from "../../../utils/functions"

function CharacteristicDurationsTableLine({getFunction, label, colored = false}) {
	return (
		<tr>
			<Td label={"Type"}>
				<div className={"text-base"}>
					{label}
				</div>
			</Td>
			<Td label={"SSOL"}>
				{getFormattedDuration(getFunction("ssol_all"))}
			</Td>
			<Td label={"SB"}>
				{getFormattedDuration(getFunction("sb"))}
			</Td>
			<Td label={"CB"}>
				{getFormattedDuration(getFunction("cb"))}
			</Td>
		</tr>
	)
}

export const CharacteristicDurationsTableLineMemo = React.memo(CharacteristicDurationsTableLine)