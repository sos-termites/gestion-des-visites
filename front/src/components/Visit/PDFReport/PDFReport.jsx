/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useEffect, useState} from 'react'
import {base64ToBlob, getPDFReport} from "../../../services/validation"
import Spinner from "../../Spinner"
import {useFetchAPI} from "../../../services/authentication"

export default function PDFReport({visit, token, setIsButtonAvailable}) {

	/**
	 * Local state which stores pdfURL
	 */
	const [pdfURL, setPDFURL] = useState(null)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Effect used to load new PDF when the component mounts
	 */
	useEffect(async () => {
		setIsButtonAvailable(false)
		const {pdf: base64} = await getPDFReport(API, token, visit.id)
		const blob = base64ToBlob(base64, 'application/pdf')
		setPDFURL(URL.createObjectURL(blob))
		setIsButtonAvailable(true)
	}, [API])

	return (
		pdfURL ? <iframe width="100%" src={pdfURL} className={"pdf-report"}/> :
			<div className={"flex justify-center"}>
				<Spinner className={"h-16 w-16 text-red-600"}/>
			</div>

	)
}
