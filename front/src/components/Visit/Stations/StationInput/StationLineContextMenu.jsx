/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useMemo} from "react"
import ThreeDots from "../../../../Icons/ThreeDots"
import ContextMenuItemWithIcon from "../../../ContextMenu/ContextMenuItemWithIcon"
import Recharge from "../../../../Icons/Recharge"
import Replace from "../../../../Icons/Replace"
import Cancel from "../../../../Icons/Cancel"
import ContextMenu from "../../../ContextMenu/ContextMenu"
import {DocumentDuplicateIcon, LockClosedIcon, LockOpenIcon} from "@heroicons/react/solid"
import StationLineContextMenuItem from "./StationLineContextMenuItem"
import Lid from "../../../../Icons/Lid"
import {getIndexNameArray} from "../../../../services/stations"
import RemoveStationContextMenuItem from "./ContextMenuItems/RemoveStationContextMenuItem"

export default function StationLineContextMenu({
	                                               station,
	                                               index,
	                                               updateStationLine,
	                                               deleteStationLine,
	                                               setNewStationNumber,
	                                               setisLastAddedLineStationNumberChosen,
	                                               manuallyAdded,
	                                               event_id,
	                                               type,
	                                               stationsReplica,
	                                               setIsPopupVisible,
	                                               setPopupsOnClick
                                               }) {
	/**
	 * Station's excluded attributes for station status generation
	 * @type {string[]}
	 */
	const excludedAttributes = [
		"isInActivity",
		"isConnected",
		"isDuplicated"
	]

	/**
	 * All station status attributes
	 * @type {string[]}
	 */
	const allAttributes = useMemo(() => {
		return Object.keys(station).filter(d => d.includes('is') && !excludedAttributes.includes(d))
	}, [station, excludedAttributes])


	return (
		<ContextMenu openingButton={<div className={"p-2 bg-indigo-500 rounded shadow-md text-white"}>
			<ThreeDots/>
		</div>}
		             buttonClassName={"focus:ring-indigo-500"}>

			<RemoveStationContextMenuItem station={station} index={index} updateStationLine={updateStationLine}
			                              event_id={event_id} type={type} setIsPopupVisible={setIsPopupVisible}
			                              allAttributes={allAttributes} setPopupsOnClick={setPopupsOnClick}/>


			<StationLineContextMenuItem station={station} index={index} className={"text-green-600"} type={type}
			                            event_id={event_id} icon={<Recharge width={20}/>} attribute={"isRecharged"}
			                            label={"Recharger"} cancelLabel={'Annuler le rechargement'}
			                            excludedAttributes={"isLidChanged"}
			                            attributes={allAttributes} updateStationLine={updateStationLine}/>

			<StationLineContextMenuItem station={station} index={index} className={"text-blue-600"} type={type}
			                            event_id={event_id} icon={<Replace width={20}/>} attribute={"isReplaced"}
			                            label={"Remplacer"} cancelLabel={'Annuler le remplacement'}
			                            excludedAttributes={"isNotAccessible"}
			                            attributes={allAttributes} updateStationLine={updateStationLine}/>

			{type === "ssol" &&
				<StationLineContextMenuItem station={station} index={index} className={"text-purple-600"} type={type}
				                            event_id={event_id} icon={<Lid width={20}/>} attribute={"isLidChanged"}
				                            label={"Changer le couvercle"}
				                            cancelLabel={'Annuler le changement de couvercle'}
				                            excludedAttributes={"isRecharged"}
				                            attributes={allAttributes} updateStationLine={updateStationLine}/>}

			{type === "sb" &&
				<StationLineContextMenuItem station={station} index={index} className={"text-purple-600"} type={type}
				                            event_id={event_id} icon={<DocumentDuplicateIcon width={20}/>}
				                            attribute={"isDuplicated"}
				                            label={"Dupliquer"}
				                            cancelLabel={null}
				                            unitAction={true}
				                            disabledCondition={stationsReplica && stationsReplica.length >= getIndexNameArray().length}
				                            attributes={allAttributes} updateStationLine={updateStationLine}/>}

			<StationLineContextMenuItem station={station} index={index} className={"text-yellow-600"} type={type}
			                            event_id={event_id} icon={station.isNotAccessible ? <LockOpenIcon width={20}/> :
				<LockClosedIcon width={20}/>} attribute={"isNotAccessible"}
			                            label={"Non accessible"} cancelLabel={'Annuler la non accessiblité'}
			                            attributes={allAttributes} updateStationLine={updateStationLine}/>
			{manuallyAdded &&
				<ContextMenuItemWithIcon icon={<Cancel width={20}/>}
				                         onClick={() => {
					                         if (station.isNew && !station.masterStation) {
						                         setNewStationNumber(c => c - 1)
					                         }
					                         if (station.manuallyAdded) {
						                         setisLastAddedLineStationNumberChosen(true)
					                         }
					                         deleteStationLine({
						                         event_id: event_id,
						                         type: type,
						                         index: index,
						                         number: station.stationNumber,
						                         station: station
					                         })
				                         }}
				                         className={"text-gray-600"}>
					<div
						className={"font-semibold"}>{station.isNew ? "Annuler l'installation" : "Pas d'observation"}</div>
				</ContextMenuItemWithIcon>}
		</ContextMenu>
	)
}
