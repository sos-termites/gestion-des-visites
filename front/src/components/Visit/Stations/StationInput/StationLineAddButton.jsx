/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import Td from "../../../Table/Td"
import IconButton from "../../../IconButton"
import Plus from "../../../../Icons/Plus"

export default function StationLineAddButton({addLine, disabled}) {
	return (
		<tr key={"plus"}>
			<Td colspan={"7"}>
				<IconButton icon={<Plus/>} onClick={addLine} disabled={disabled}
				            className={"text-white p-2 bg-blue-500 rounded shadow-md focus:ring-offset-gray-100 focus:ring-blue-500"}/>
			</Td>
		</tr>
	)
}
