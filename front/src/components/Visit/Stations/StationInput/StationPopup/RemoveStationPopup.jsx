/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from 'react'
import StationPopup from "./StationPopup"

export default function RemoveStationPopup({
	                                           isPopupVisible,
	                                           setIsPopupVisible,
	                                           popupsOnClick
                                           }) {
	const name = "removeStation"

	return (
		<StationPopup name={name} popupOpen={isPopupVisible} setPopupOpen={setIsPopupVisible}
		              title={"Type de démontage"} buttons={[
			{
				label: "Temporaire",
				className: "bg-yellow-600 hover:bg-yellow-500 focus:ring-yellow-500",
				onClick: popupsOnClick[name][0]
			},
			{
				label: "Définitif",
				className: "bg-red-600 hover:bg-red-500 focus:ring-red-500",
				onClick: popupsOnClick[name][1]
			}
		]}>
			<p className="text-base text-gray-500">
				Merci de choisir le type de démontage de cette station
			</p>
		</StationPopup>
	)
}
