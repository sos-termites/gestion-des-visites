/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useEffect, useMemo, useState} from "react"
import StationInputBurger from "./ConsumptionHistoryBurger"
import Table from "../../../Table/Table"

import './ConsumptionHistory.css'
import {ConsumptionHistoryLineMemo} from "./ConsumptionHistoryLine"
import {getConsumptionHistory} from "../../../../services/stations"
import ConsumptionHistoryLabelItem from "./ConsumptionHistoryLabelItem"
import {useFetchAPI} from "../../../../services/authentication"


export default function ConsumptionHistory({token, worksite, type, setIsHistoryVisible, getStatus}) {

	const [visits, setVisits] = useState([])
	const [consumption, setConsumption] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [seeAll, setSeeAll] = useState(false)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Effect used to load consumption history from the API
	 */
	useEffect(() => {
		setIsLoading(true)
		getConsumptionHistory(API, token, worksite.id).then(/** @param response {{visits: Array, stations: Array}}*/response => {
			setVisits(response.visits)
			setConsumption(response.stations)
			setIsLoading(false)
		})
	}, [API])

	/**
	 * Labels of the table
	 * @type {Array}
	 */
	const visitsLabel = useMemo(() => [
		"N°",
		...visits.map(v => <ConsumptionHistoryLabelItem visit={v}/>),
		<StationInputBurger setIsHistoryVisible={setIsHistoryVisible} setSeeAll={setSeeAll} seeAll={seeAll}/>
	], [visits, setIsHistoryVisible, setSeeAll, seeAll])


	/**
	 * Lines of the table
	 * @type {Array}
	 */
	const lines = consumption.map(c => {
		const invisible = c.history.every(status => status === false)
		const number = `${c.number}${c.indexName ? ` ${c.indexName}` : ''}`
		return (!invisible || seeAll) && type === c.type.short_name &&
			<ConsumptionHistoryLineMemo key={number} number={number} history={c.history} getStatus={getStatus}/>
	})

	return (
		!isLoading && <Table className={"rounded mb-4"}
		                     ths={visitsLabel}
		                     trs={lines}
		/>
	)
}

