/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function NumberInput({min, max, step, className, value, onChange}) {
	return (
		<input type={"number"} min={min} max={max} step={step} value={value} onChange={onChange}
		       className={`inline mr-1 focus:ring-indigo-500 focus:border-indigo-500 block ${className} shadow-sm sm:text-sm border-gray-300 rounded-md`}/>
	)
}
