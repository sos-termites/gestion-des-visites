/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useEffect, useRef} from "react"
import './Table.css'
import Th from "./Th"

export default function Table({ths, trs, className}) {

	/**
	 * Reference to an anchor div
	 * @type {React.MutableRefObject<HTMLDivElement>}
	 */
	const divAnchor = useRef()

	/**
	 * Effect used to scroll automatically into the end of the table
	 */
	useEffect(() => {
		if (divAnchor.current instanceof HTMLDivElement) {
			divAnchor.current.scrollIntoView({block: 'nearest', inline: 'start'})
		}
	}, [])

	return (
		<table className={`min-w-full leading-normal table-auto table-responsive border-2 ${className}`}>
			<thead>
			<tr>
				{ths.map((label, index) => label && <Th key={index}>{label}</Th>)}
				<div ref={divAnchor}/>
			</tr>
			</thead>
			<tbody>
			{trs}
			</tbody>
		</table>
	)
}
