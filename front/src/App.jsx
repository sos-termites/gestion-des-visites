/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useEffect, useRef, useState} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import Nav from './components/Nav'

import DashboardPage from './views/DashboardPage'
import NotFound from './views/NotFound'
import Login from "./views/Login"
import {getUser} from "./services/authentication"
import Loading from "./views/Loading"
import VisitPage from "./views/VisitPage"

const App = () => {
	const token = useRef()
	const [user, setUser] = useState(null)
	const [loading, setLoading] = useState(true)
	const [logged, setLogged] = useState(false)

	useEffect(() => {
		if (!user) {
			getUser(token, setUser, setLoading)
		}
	}, [logged])

	if (loading) {
		return <Loading/>
	}

	if (!user) {
		return <Login ref={token} state={{setLogged, setLoading}}/>
	}

	return (
		<Router>
			<Nav/>
			<Switch>
				<Route exact path="/">
					<DashboardPage token={token} setLogged={setLogged}/>
				</Route>
				<Route path="/dashboard">
					<DashboardPage token={token} setLogged={setLogged}/>
				</Route>
				<Route path="/report/:event_id">
					<VisitPage token={token}/>
				</Route>
				<Route path="/login">
					<Login/>
				</Route>
				<Route>
					<NotFound/>
				</Route>
			</Switch>
		</Router>
	)
}

export default App
