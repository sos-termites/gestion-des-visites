/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

/**
 * Finds the last number in steps
 * @param steps
 * @returns {Array}
 */
export function findLastNumber(steps) {
	const stepsCopy = [
		...steps
	]
	return stepsCopy.sort((a, b) => {
		return b.number - a.number
	})?.[0]?.number
}

/**
 * Finds the index of a step whose a specific parameter is true
 * @param steps {Array} Visit report steps
 * @param param {String} Parameter name
 * @returns {null|Number}
 */
export function findIndex(steps, param) {
	const index = steps.indexOf(steps.filter(s => s[param])?.[0])
	if (index > -1) {
		return index
	}
	return null
}

/**
 * Gets the initial steps
 * @returns {Array}
 */
export function getInitialSteps() {
	return [
		{
			number: 1,
			title: "Stations",
			description: "Maintenance et entretien",
			status: "progress",
			isEliminationBegin: false,
			isEliminationEnd: false,
			components: ["StationPage"],
		},
		{
			number: 2,
			title: "Observations",
			description: "Observations diverses et internes",
			status: "next",
			isEliminationBegin: false,
			isEliminationEnd: false,
			components: ["ObservationPage"],
		},
		{
			number: 3,
			title: "Prochaine visite",
			description: "Type et date de la prochaine visite",
			status: "next",
			isEliminationBegin: false,
			isEliminationEnd: false,
			components: ["NextVisitPage"],
		}
	]
}
