/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useEffect, useState} from 'react'
import {useParams} from "react-router-dom"
import Header from '../components/Header'
import DashboardItem from "../components/DashboardItem"
import {searchWorksites} from "../services/visit"
import Visit from "../Icons/Visit"
import IconButton from "../components/IconButton"
import Page from "../components/Visit/Pages/Page"
import {useFetchAPI} from "../services/authentication"


export default function VisitPage({token}) {
	const [visit, setVisit] = useState(null)
	const [showBar, setShowBar] = useState(false)

	const {event_id} = useParams()

	const API = useFetchAPI()

	useEffect(() => {
		searchWorksites(API, token, event_id).then(setVisit)
	}, [])

	return (
		<React.Fragment>
			<main
				className={"bg-gray-100 dark:bg-gray-800 flex flex-col lg:grid w-full visit-page-grid fixed overflow-y-scroll"}>
				<div className={"sidebar hidden lg:flex"}>
					{visit && <DashboardItem worksite={visit} isVisit={true}/>}
				</div>
				<div className={"ml-4 lg:ml-0 my-2 pr-4 w-full"}>
					<Header icon={<IconButton icon={<Visit/>} onClick={() => setShowBar(!showBar)}/>}>
						Compte rendu de visite
					</Header>
					{visit && !showBar && <Page visit={visit} event_id={event_id} token={token} setVisit={setVisit}/>}
				</div>
				<div className={"lg:hidden mt-4"}>
					{visit && showBar && <DashboardItem worksite={visit} isVisit={true}/>}
				</div>
			</main>
		</React.Fragment>
	)
}
