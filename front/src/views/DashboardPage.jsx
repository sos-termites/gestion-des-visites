/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback, useEffect, useState} from 'react'
import Header from '../components/Header'
import DashboardItem from "../components/DashboardItem"
import SearchBar from "../components/SearchBar"
import {getScheduledVisits, searchWorksites} from "../services/dashboard"
import {getAPIFormattedDate} from "../utils/functions"
import DashBoardTitleDate from "../components/Dashboard/DashBoardTitleDate"
import Loading from "../components/Dashboard/Loading"
import {useDebouncedCallback} from "use-debounce"
import DashBoardTitleSearch from "../components/Dashboard/DashBoardTitleSearch"
import useAbortableEffect from "@closeio/use-abortable-effect"
import DatePicker from "../components/DatePicker/DatePicker"
import {useFetchAPI} from "../services/authentication"

const DashboardPage = ({token}) => {

	const [date, setDate] = useState(new Date())
	const [searchedDate, setSearchedDate] = useState(date)
	const [worksites, setWorksites] = useState({loading: false, value: []})
	const [search, setSearch] = useState("")

	const API = useFetchAPI()

	/**
	 *
	 * @type {function(string): void}
	 */
	const debouncedSearch = useDebouncedCallback(value => setSearch(value), 400)
	/**
	 *
	 * @type {function(string): void}
	 */
	const debouncedDate = useDebouncedCallback(value => setSearchedDate(value), 300)

	const changeDateDebounced = useCallback((date) => {
		setDate(date)
		debouncedDate(date)
	}, [])

	const changeDate = useCallback((date) => {
		setDate(date)
		setSearchedDate(date)
	}, [])

	/**
	 * Effect used to load worksites from a specific date
	 */
	useEffect(() => {
		if (search === "") {
			const fetchWorksites = async () => {
				setWorksites({loading: true, value: []})
				const data = await getScheduledVisits(API, token, getAPIFormattedDate(searchedDate))
				if (data instanceof Array) {
					setWorksites({loading: false, value: data})
				}
			}
			fetchWorksites().catch(console.error)
		}
	}, [API, searchedDate, search])

	/**
	 * Effect used to search for specific worksites
	 */
	useAbortableEffect((abortSignal) => {
		if (search !== "") {
			setWorksites({loading: true, value: []})
			searchWorksites(API, token, search, abortSignal).then(data => {
				if (data instanceof Array) {
					setWorksites({loading: false, value: data})
				}
			})
		}
		return (controller) => {
			controller.abort()
		}
	}, [API, search])

	return (
		<React.Fragment>
			<Header title="Tableau de bord"/>
			<main className={"bg-gray-100 dark:bg-gray-800"}>
				<div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
					<SearchBar placeholder={"Rechercher un chantier..."} onChange={e => debouncedSearch(e.target.value)}
					           isSearched={search !== ""} onClickCancel={() => setSearch("")}/>
				</div>
				<div className="max-w-7xl mx-auto py-2 sm:px-6 lg:px-8">
					{search === "" ? <DashBoardTitleDate date={date} setDate={changeDateDebounced}
					                                     datepicker={<DatePicker selected={date}
					                                                             onChange={changeDate}/>}/> :
						<DashBoardTitleSearch search={search}/>}
				</div>
				<div className="max-w-7xl mx-auto py-2 sm:px-6 lg:px-8">
					{worksites.loading && <Loading/>}
				</div>
				<div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
					{!worksites.loading && worksites.value && worksites.value.map(
						worksite => <DashboardItem worksite={worksite}
						                           key={worksite?.worksite?.code.toString() || worksite.code.toString()}/>)}
				</div>
			</main>
		</React.Fragment>
	)
}

export default DashboardPage
