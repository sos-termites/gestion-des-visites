/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function Previous({width, height}) {
	return (
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
		     x="0px" y="0px"
		     viewBox="0 0 492.672 492.672" xmlSpace="preserve" width={width} height={height} fill="currentColor">
			<g>
				<g>
					<path d="M469.62,92.116c-6.5,0-13.232,2.616-20.452,7.596l-169.392,116.9c-11.308,7.804-17.54,18.36-17.54,29.728
			c0,11.364,6.232,21.912,17.54,29.728l169.392,116.88c7.22,4.968,13.956,7.612,20.468,7.604c14.288,0,23.036-12.304,23.036-31.336
			v-245.76C492.672,104.424,483.924,92.116,469.62,92.116z"/>
				</g>
			</g>
			<g>
				<g>
					<path d="M207.412,92.12c-6.504,0-13.26,2.616-20.472,7.596l-169.392,116.9C6.236,224.42,0,234.976,0,246.344
			c0,11.364,6.236,21.912,17.548,29.728L186.94,392.948c7.208,4.968,13.968,7.612,20.48,7.608c14.308,0,23.06-12.304,23.06-31.336
			V123.46C230.48,104.428,221.728,92.12,207.412,92.12z"/>
				</g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
		</svg>
	)
}
