<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\ScheduledVisit;
use App\Entity\Visit;
use App\Entity\Worksite;
use App\Service\GoogleCalendarService;
use App\Service\ScheduledVisitService;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Google\Service\Calendar\Event;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;

class ScheduledVisitDataProvider extends AbstractController implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface, ItemDataProviderInterface
{
	public function __construct(private Security $security, private GoogleCalendarService $calendarService, private ScheduledVisitService $scheduledVisitService, private ManagerRegistry $doctrine)
	{
	}

	/**
	 * @throws Exception
	 * Gets worksites collection corresponding to scheduled events during a specific day for a specific user.
	 */
	public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
	{
		$user = $this->security->getUser();
		if (!$user) {
			throw new HttpException(401, "Please authenticate yourself");
		}
		$date = $context["filters"]["date"];
		$results = $this->calendarService->getScheduledEvents($user, new DateTime($date));
		/**
		 * @var $events ?Event[]
		 */
		$events = $results["items"];
		$scheduledVisits = [];
		if ($events) {
			foreach ($events as $event) {
				if ($scheduledVisit = $this->getScheduledVisit($event)) {
					$scheduledVisits[] = $scheduledVisit;
				}
			}
		}
		return $scheduledVisits;
	}

	/**
	 * @param Event $event Google Calendar Event
	 * @return ScheduledVisit|null The generated scheduled visit corresponding to the event
	 * @throws Exception
	 * Generates a scheduled visit thanks to an event on the calendar
	 */
	private function getScheduledVisit(Event $event): ?ScheduledVisit
	{
		$worksite = $this->getWorksite($event);
		if ($worksite) {
			$doneVisit = $this->getVisit($event);
			$type = $doneVisit ? $doneVisit->getType() : $worksite->getNextVisitType();
			return $this->scheduledVisitService->getScheduledVisitFromEvent($event, $worksite, $type, $doneVisit);
		}
		return null;
	}

	/**
	 * @param Event $event Google Calendar Event
	 * @return Worksite|null The corresponding worksite or null
	 * Gets the corresponding worksite of an event, using worksite internal id which is on the first line of the event description
	 */
	private function getWorksite(Event $event): ?Worksite
	{
		$description = $event->getDescription();
		$code = strstr($description, "<br>", true);
		if (!$code) {
			$code = strstr($description, "\n", true);
		}
		$worksite = $this->doctrine->getManager()->getRepository(Worksite::class)->findOneBy(['code' => $code]);
		if ($worksite instanceof Worksite) {
			return $worksite;
		}
		return null;
	}

	/**
	 * @param Event $event Google Calendar Event
	 * @return Visit|null The corresponding visit if it has been created or null if not
	 * Get the corresponding visit of an event (or nnull if there is none)
	 */
	private function getVisit(Event $event): ?Visit
	{
		$visit = $this->doctrine->getManager()->getRepository(Visit::class)->findOneBy(["eventId" => $event->getId()]);
		if ($visit instanceof Visit) {
			return $visit;
		}
		return null;
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return $resourceClass === ScheduledVisit::class;
	}

	/**
	 * @throws Exception
	 */
	public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?ScheduledVisit
	{
		$user = $this->security->getUser();
		if (!$user) {
			throw new HttpException(401, "Please authenticate yourself");
		}
		$event = $this->calendarService->getEvent($user, $id);
		if ($event) {
			return $this->getScheduledVisit($event);
		}
		return null;
	}
}

