<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\VisitStepType;
use App\Entity\VisitStepTypeAdjustment;
use App\Entity\Worksite;
use App\Repository\VisitStepTypeRepository;
use App\Service\StepDurationCalculationService;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;

class VisitStepTypeDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
	public function __construct(private StepDurationCalculationService $durationCalculationService, private EntityManagerInterface $entityManager)
	{

	}

	/**
	 * Finds corresponding visit types and hydrates them with visit step durations when worksite_id exists
	 * @param string $resourceClass
	 * @param string|null $operationName
	 * @param array $context
	 * @return array
	 */
	public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
	{
		$repository = $this->entityManager->getRepository(VisitStepType::class);
		if ($repository instanceof VisitStepTypeRepository) {
			$collection = $repository->findAll();

			if (isset($context["filters"]["worksite_id"])) {
				$worksite = $this->entityManager->getRepository(Worksite::class)->find($context["filters"]["worksite_id"]);
				if ($worksite instanceof Worksite) {
					return array_map(function ($v) use ($worksite) {
						if ($v instanceof VisitStepType) {
							[$originalDuration, $adjustmentDuration] = $this->durationCalculationService->getCharacteristicDurations($v, $worksite, DateInterval::createFromDateString("0 min"));
							return $v->removeAllAdjustments()->setDuration($originalDuration ?? DateInterval::createFromDateString("0 min"))
								->addAdjustment((new VisitStepTypeAdjustment())->setDuration($adjustmentDuration ?? DateInterval::createFromDateString("0 min"))->setId(0));
						}
						return null;
					}, $collection);
				}
			}
			return $collection;
		}
		return [];
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return $resourceClass === VisitStepType::class;
	}
}
