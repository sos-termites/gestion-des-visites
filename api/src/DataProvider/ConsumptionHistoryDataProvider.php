<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Worksite;
use App\Repository\WorksiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class ConsumptionHistoryDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{

	public function __construct(private EntityManagerInterface $entityManager)
	{
	}

	/**
	 * @throws NonUniqueResultException
	 */
	public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Worksite
	{
		$repository = $this->entityManager->getRepository($resourceClass);
		if ($repository instanceof WorksiteRepository) {
			return $repository->findByIdWithOrderedVisits($id);
		}
		return null;
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return $resourceClass === Worksite::class && ($operationName === 'get_consumption_history' || $operationName === 'get_elimination_end_info');
	}
}
