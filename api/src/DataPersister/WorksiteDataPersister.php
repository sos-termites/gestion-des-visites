<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Worksite;
use App\Service\TypesenseService;
use Http\Client\Exception;
use Typesense\Exceptions\TypesenseClientError;

class WorksiteDataPersister implements ContextAwareDataPersisterInterface
{

	public function __construct(private ContextAwareDataPersisterInterface $decoratedContextAwareDataPersister, private TypesenseService $typesenseService)
	{
	}

	public function supports($data, array $context = []): bool
	{
		return $data instanceof Worksite && $this->decoratedContextAwareDataPersister->supports($data, $context);
	}

	/**
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function persist($data, array $context = []): object
	{
		$result = $this->decoratedContextAwareDataPersister->persist($data, $context);
		if ((($context["collection_operation_name"] ?? null) === 'post' ||
			($context["item_operation_name"] ?? null) === 'put' ||
			($context["item_operation_name"] ?? null) === 'patch')) {
			$this->typesenseService->upsertWorksite($data);
		}
		return $result;
	}

	/**
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function remove($data, array $context = [])
	{
		$id = $data->getId();
		$result = $this->decoratedContextAwareDataPersister->remove($data, $context);
		if (($context["item_operation_name"] ?? null) === 'delete') {
			$this->typesenseService->deleteWorksite($id);
		}
		return $result;
	}
}
