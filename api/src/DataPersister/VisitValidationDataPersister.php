<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\NextScheduledVisit;
use App\Entity\Visit;
use App\Service\GoogleCalendarService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class VisitValidationDataPersister implements ContextAwareDataPersisterInterface
{

	public function __construct(private ContextAwareDataPersisterInterface $decoratedContextAwareDataPersister, private GoogleCalendarService $calendarService, private EntityManagerInterface $entityManager)
	{
	}

	public function supports($data, array $context = []): bool
	{
		return $data instanceof Visit && (($context["item_operation_name"] ?? null) === "validate_visit") && $this->decoratedContextAwareDataPersister->supports($data, $context);
	}

	/**
	 * Persists data and creates a Google Calendar event only if the visit has not already been validated
	 * @throws Exception
	 */
	public function persist($data, array $context = []): object
	{
		if ($data instanceof Visit) {
			if ($data->getIsCompleted()) {
				$nextVisit = $this->entityManager->getRepository(NextScheduledVisit::class)->findOneBy(["previousVisit" => $data->getId()]);
				if ($nextVisit instanceof NextScheduledVisit) {
					$this->calendarService->createEvent($nextVisit);
					return $this->decoratedContextAwareDataPersister->persist($data);
				}
			} else {
				throw new HttpException(Response::HTTP_FORBIDDEN, "Une visite validée ne peut pas changer de statut");
			}

		}
		throw new Exception("L'objet fourni n'est pas une entité Visite");
	}


	public function remove($data, array $context = [])
	{
		$this->decoratedContextAwareDataPersister->remove($data, $context);
	}
}
