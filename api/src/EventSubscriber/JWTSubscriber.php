<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\EventSubscriber;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class JWTSubscriber implements EventSubscriberInterface
{
	public function __construct(private EntityManagerInterface $entityManager)
	{
	}

	public static function getSubscribedEvents(): array
	{
		return [
			'lexik_authentication.on_jwt_created' => 'onLexikAuthenticationOnJwtCreated',
			'lexik_jwt_authentication.on_jwt_authenticated' => 'onLexikAuthenticationOnJwtAuthenticated',
		];
	}

	public function onLexikAuthenticationOnJwtCreated(JWTCreatedEvent $event)
	{
		$data = $event->getData();
		$user = $event->getUser();
		if ($user instanceof User) {
			$data["email"] = $user->getUserIdentifier();
			$data["calendar_id"] = $user->getCalendarId();
			$data["id"] = $user->getId();
		}
		$event->setData($data);
	}

	public function onLexikAuthenticationOnJwtAuthenticated(JWTAuthenticatedEvent $event)
	{
		$token = $event->getToken();
		$user = $token->getUser();
		if ($user instanceof User) {
			$this->entityManager->getUnitOfWork()->createEntity(User::class, [
				'id' => $user->getId(),
			]);
			$user = $this->entityManager->merge($user); //TODO: Replace
			$token->setUser($user);
		}
	}
}
