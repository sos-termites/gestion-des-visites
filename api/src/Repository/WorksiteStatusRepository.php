<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\WorksiteStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorksiteStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorksiteStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorksiteStatus[]    findAll()
 * @method WorksiteStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorksiteStatusRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, WorksiteStatus::class);
	}

	// /**
	//  * @return WorksiteStatus[] Returns an array of WorksiteStatus objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('w')
			->andWhere('w.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('w.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?WorksiteStatus
	{
		return $this->createQueryBuilder('w')
			->andWhere('w.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
