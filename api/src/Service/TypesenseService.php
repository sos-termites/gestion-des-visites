<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Service;


use App\Entity\Worksite;
use Doctrine\ORM\EntityManagerInterface;
use Http\Client\Exception;
use JsonException;
use Symfony\Component\HttpClient\HttplugClient;
use Typesense\Client;
use Typesense\Exceptions\ConfigError;
use Typesense\Exceptions\TypesenseClientError;

class TypesenseService
{

	private Client $client;

	/**
	 * @throws ConfigError
	 */
	public function __construct(private string $typesenseKey, private EntityManagerInterface $entityManager)
	{
		$this->setClient();
	}

	/**
	 * @throws ConfigError
	 * Create typesense client
	 */
	private function setClient()
	{
		$this->client = new Client([
			'api_key' => $this->typesenseKey,
			'nodes' => [
				[
					'host' => 'typesense',
					'port' => '8108',
					'protocol' => 'http',
				],
			],
			'client' => new HttplugClient(),
		]);
	}

	/**
	 * Creates worksite collection
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function createWorksiteCollection()
	{
		$this->client->collections->create([
			'name' => 'worksites',
			'fields' => [
				[
					'name' => 'code',
					'type' => 'string',
				],
				[
					'name' => 'name',
					'type' => 'string',
				],
				[
					'name' => 'address',
					'type' => 'string',
				],
			],
		]);
	}

	/**
	 * Import all worksites that are stored in database
	 * @throws Exception
	 * @throws TypesenseClientError
	 * @throws JsonException
	 */
	public function importAllWorksites()
	{
		$worksites = $this->entityManager->getRepository(Worksite::class)->findAll();
		$worksites = array_map(fn(Worksite $w) => $this->normalizeWorksite($w), $worksites);
		$this->client->collections["worksites"]->documents->import($worksites, ['action' => 'create']);
	}

	/**
	 * Normalizes a worksite to index it as a typesense document
	 * @param Worksite $worksite The worksite to normalize
	 * @return array Normalized array
	 * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
	 * @noinspection PhpPureAttributeCanBeAddedInspection
	 */
	private function normalizeWorksite(Worksite $worksite): array
	{
		return [
			"id" => strval($worksite->getId()),
			"name" => $worksite->getName(),
			"code" => strval($worksite->getCode()),
			"address" => $worksite->getAddress()->getStreet() . " " . $worksite->getAddress()->getCity()
		];
	}

	/**
	 * Upsert (insert or update) the specified worksite into Typesense database
	 * @param Worksite $worksite The worksite to upsert
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function upsertWorksite(Worksite $worksite)
	{
		$this->client->collections["worksites"]->documents->upsert($this->normalizeWorksite($worksite));
	}

	/**
	 * Deletes the specified worksite from typesense database
	 * @param string $id Worksite id as a string
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function deleteWorksite(string $id)
	{
		$this->client->collections["worksites"]->documents[$id]->delete();
	}

	/**
	 * Searches for worksites corresponding to the query in Typesense database
	 * @param string $query Search query
	 * @return array Array of corresponding worksites
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function searchWorksite(string $query): array
	{
		$worksites = $this->client->collections["worksites"]->documents->search(
			[
				'q' => $query,
				'query_by' => 'name, code, address',
				'query_by_weights' => "4, 4, 1",
				'num_typos' => '1',
				'per_page' => '5'
			]
		);
		return array_map(fn($w) => $w["document"]["id"], $worksites["hits"]);
	}
}

