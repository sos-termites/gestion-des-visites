<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Service;


use App\Entity\ScheduledVisit;
use App\Entity\Visit;
use App\Entity\VisitType;
use App\Entity\Worksite;
use DateInterval;
use DateTime;
use Exception;
use Google\Service\Calendar\Event;


class ScheduledVisitService
{
	public function __construct(private GoogleCalendarService $calendarService, private StepDurationCalculationService $durationCalculationService)
	{
	}

	/**
	 * Gets next scheduled visits of a specific worksite
	 * @param Worksite $worksite Concerned worksite
	 * @return array
	 * @throws Exception
	 */
	public function getNextScheduledVisits(Worksite $worksite): array
	{
		$events = $this->calendarService->getWorksiteEvents($worksite->getCode(), new DateTime(), true);
		return $events->map(fn($e) => $this->getScheduledVisitFromEvent($e, $worksite, $worksite->getNextVisitType(), null))->toArray();
	}

	/**
	 * Constructs a ScheduledVisit object from an event, a visit type and a worksite
	 * @param Event $event Event
	 * @param Worksite $worksite Concerned worksite
	 * @param VisitType $type Type of the visit
	 * @param Visit|null $visit Concerned visit (if it exists)
	 * @return ScheduledVisit
	 * @throws Exception
	 */
	public function getScheduledVisitFromEvent(Event $event, Worksite $worksite, VisitType $type, ?Visit $visit): ScheduledVisit
	{
		$start = new DateTime($event->getStart()->getDateTime());
		$end = new DateTime($event->getEnd()->getDateTime());
		$updatedType = $this->getTypeWithCalculatedStepDuration($type, $worksite, $start->diff($end));
		return (new ScheduledVisit())
			->setEventId($event->getId())
			->setScheduledAt($start)
			->setScheduledDuration($start->diff($end))
			->setWorksite($worksite)
			->setType($updatedType)
			->setVisit($visit);
	}

	/**
	 * Sets calculated step durations to visitType object
	 * @param VisitType $type Type of the visit
	 * @param $worksite Worksite Concerned worksite
	 * @param $duration DateInterval Total duration of the visit
	 */
	public function getTypeWithCalculatedStepDuration(VisitType $type, Worksite $worksite, DateInterval $duration): VisitType
	{
		return $this->durationCalculationService->calculateStepsDuration($type, $worksite, $duration);
	}
}

