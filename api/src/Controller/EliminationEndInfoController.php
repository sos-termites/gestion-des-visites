<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;


use App\Entity\EliminationPeriod;
use App\Entity\StationStatus;
use App\Entity\Visit;
use App\Entity\Worksite;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EliminationEndInfoController extends AbstractController
{
	public function __construct()
	{
	}

	/**
	 * @throws Exception
	 */
	public function __invoke(Worksite $data): array
	{
		$eliminationPeriod = $data->getEliminationPeriods()->filter(fn(EliminationPeriod $p) => $p->getEndVisit() === null)->first();
		if ($eliminationPeriod instanceof EliminationPeriod) {
			$visits = $data->getVisits();
			$visitsSinceEliminationBegin = $visits->slice($visits->indexOf($eliminationPeriod->getStartVisit()));
			$visitsWithoutConsumption = [];
			$lastVisitWithConsumption = $eliminationPeriod->getStartVisit();
			$i = 0;
			while (count($visitsWithoutConsumption) < 2 && $i < count($visitsSinceEliminationBegin) - 1) {
				if ($this->hasConsumptionIncreased($visitsSinceEliminationBegin[$i], $visitsSinceEliminationBegin[$i + 1])) {
					$visitsWithoutConsumption[] = $visitsSinceEliminationBegin[$i + 1];
				} else {
					$visitsWithoutConsumption = [];
					$lastVisitWithConsumption = $visitsSinceEliminationBegin[$i + 1];
				}
				$i++;
			}
			return [
				"consumptionBegin" => $eliminationPeriod->getStartVisit()->getScheduledAt(),
				"consumptionEnd" => $lastVisitWithConsumption->getScheduledAt(),
				"inactivityBegin" => $visitsWithoutConsumption[0] ? $visitsWithoutConsumption[0]->getScheduledAt() : null,
				"inactivityEnd" => $visitsWithoutConsumption[1] ? $visitsWithoutConsumption[1]->getScheduledAt() : null
			];
		}
		return [];
	}

	/**
	 * Tells if consumption has increased between two visits
	 * @param Visit $v1 Visit 1
	 * @param Visit $v2 Visit 2
	 * @return bool
	 */
	private function hasConsumptionIncreased(Visit $v1, Visit $v2): bool
	{
		return $v2->getStationStatuses()->forAll(function (int $currentStatusIndex) use ($v1, $v2) {
			/**
			 * @var $currentStatus StationStatus
			 */
			$currentStatus = $v2->getStationStatuses()[$currentStatusIndex];
			/**
			 * @var $oldStatus StationStatus
			 */
			$oldStatus = $v1->getStationStatuses()->filter(fn(StationStatus $st) => $st->getStation() === $currentStatus->getStation())->first();
			if ($oldStatus) {
				if ($oldStatus->getIsReplaced() || $oldStatus->getIsRecharged()) {
					return $currentStatus->getConsumption() === 0;
				}
				return $currentStatus->getConsumption() <= $oldStatus->getConsumption();
			}
			return $currentStatus->getConsumption() === 0;
		});
	}
}

