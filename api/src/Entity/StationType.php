<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StationTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=StationTypeRepository::class)
 */
#[ApiResource]
class StationType
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups('worksite_summary')]
	private $short_name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $long_name;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $control_duration;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getShortName(): ?string
	{
		return $this->short_name;
	}

	public function setShortName(string $short_name): self
	{
		$this->short_name = $short_name;

		return $this;
	}

	public function getLongName(): ?string
	{
		return $this->long_name;
	}

	public function setLongName(string $long_name): self
	{
		$this->long_name = $long_name;

		return $this;
	}

	public function getControlDuration(): ?int
	{
		return $this->control_duration;
	}

	public function setControlDuration(int $control_duration): self
	{
		$this->control_duration = $control_duration;

		return $this;
	}
}
