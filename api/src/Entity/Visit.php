<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\VisitReportController;
use DateInterval;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VisitRepository")
 */
#[ApiResource(itemOperations: [
	'get_visit_report' => [
		'method' => 'GET',
		'path' => '/visits/{id}/report',
		'controller' => VisitReportController::class,
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]],
		'security' => 'is_granted("ROLE_USER")'],
	'validate_visit' => [
		'method' => 'PUT',
		'path' => '/visits/{id}/validation',
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]],
		'security' => 'is_granted("ROLE_USER")',
		'denormalization_context' => ['groups' => ['write:validate_visit']],]],
	denormalizationContext: ['groups' => ['write:visit']])]
#[ApiFilter(SearchFilter::class, properties: ['worksite' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['scheduled_at' => 'ASC'])]
class Visit
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	#[Groups('scheduled_visit')]
	private $id;
	/**
	 * @ORM\Column(type="datetime")
	 */
	#[Groups(['scheduled_visit', 'write:visit'])]
	private $scheduledAt;
	/**
	 * @ORM\Column(type="datetime")
	 */
	#[Groups('write:visit')]
	private $createdAt;
	/**
	 * @ORM\Column(type="datetime")
	 */
	#[Groups('write:visit')]
	private $updatedAt;
	/**
	 * @ORM\Column(type="dateinterval")
	 */
	#[Groups('write:visit')]
	private $estimatedDuration;
	/**
	 * @ORM\Column(type="dateinterval")
	 */
	#[Groups('write:visit')]
	private $effectiveDuration;
	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['scheduled_visit', 'write:visit'])]
	private $isPending;
	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['scheduled_visit', 'write:validate_visit'])]
	private $isCompleted = false;
	/**
	 * @ORM\ManyToOne(targetEntity=Worksite::class, inversedBy="visits")
	 * @ORM\JoinColumn(nullable=false)
	 */
	#[Groups(['write:visit'])]
	private $worksite;
	/**
	 * @ORM\OneToMany(targetEntity=StationStatus::class, mappedBy="visit", orphanRemoval=true, cascade={"persist"})
	 */
	#[Groups('write:visit')]
	private $stationStatuses;
	/**
	 * @ORM\ManyToOne(targetEntity=VisitType::class, inversedBy="visits")
	 * @ORM\JoinColumn(nullable=true)
	 */
	#[Groups('write:visit')]
	private $type;
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups('write:visit')]
	private $eventId;
	/**
	 * @ORM\OneToMany(targetEntity=Station::class, mappedBy="installedDuringVisit")
	 */
	#[Groups('write:visit')]
	private $installedStations;
	/**
	 * @ORM\OneToOne(targetEntity=EliminationPeriod::class, mappedBy="startVisit", cascade={"persist", "remove"})
	 */
	#[Groups('write:visit')]
	private $startingEliminationPeriod;
	/**
	 * @ORM\OneToOne(targetEntity=EliminationPeriod::class, mappedBy="endVisit", cascade={"persist", "remove"})
	 */
	#[Groups('write:visit')]
	private $endingEliminationPeriod;
	/**
	 * @ORM\Column(type="text", nullable=false)
	 */
	#[Groups('write:visit')]
	private $diverseObservation = "";
	/**
	 * @ORM\Column(type="text", nullable=false)
	 */
	#[Groups('write:visit')]
	private $internObservation = "";
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	#[Groups('write:visit')]
	private $clientReportFileID;
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	#[Groups('write:visit')]
	private $InternReportFileID;
	/**
	 * @ORM\OneToMany(targetEntity=VisitStepTypeAdjustment::class, mappedBy="addedDuringVisit", orphanRemoval=true, cascade={"persist"})
	 */
	#[Groups('write:visit')]
	private $visitStepTypeAdjustments;
	#[Groups('write:visit')]
	private array $durationAdjustments = [];

	/**
	 * @ORM\OneToOne(targetEntity=NextScheduledVisit::class, mappedBy="previousVisit", cascade={"persist", "remove"})
	 */
	#[Groups(['write:visit'])]
	private $nextScheduledVisit;

	public function __construct()
	{
		$this->stationStatuses = new ArrayCollection();
		$this->installedStations = new ArrayCollection();
		$this->visitStepTypeAdjustments = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return Visit
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getScheduledAt(): ?DateTimeInterface
	{
		return $this->scheduledAt;
	}

	public function setScheduledAt(DateTimeInterface $scheduledAt): self
	{
		$this->scheduledAt = $scheduledAt;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->createdAt;
	}

	public function setCreatedAt(DateTimeInterface $createdAt): self
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getUpdatedAt(): ?DateTimeInterface
	{
		return $this->updatedAt;
	}

	public function setUpdatedAt(DateTimeInterface $updatedAt): self
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	public function getEstimatedDuration(): ?DateInterval
	{
		return $this->estimatedDuration;
	}

	public function setEstimatedDuration(DateInterval $estimatedDuration): self
	{
		$this->estimatedDuration = $estimatedDuration;

		return $this;
	}

	public function getEffectiveDuration(): ?DateInterval
	{
		return $this->effectiveDuration;
	}

	public function setEffectiveDuration(DateInterval $effectiveDuration): self
	{
		$this->effectiveDuration = $effectiveDuration;

		return $this;
	}

	public function getIsPending(): ?bool
	{
		return $this->isPending;
	}

	public function setIsPending(bool $isPending): self
	{
		$this->isPending = $isPending;

		return $this;
	}

	public function getIsCompleted(): ?bool
	{
		return $this->isCompleted;
	}

	public function setIsCompleted(bool $isCompleted): self
	{
		$this->isCompleted = $isCompleted;

		return $this;
	}

	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getStationStatuses(): Collection
	{
		return $this->stationStatuses;
	}

	public function addStationStatus(StationStatus $stationStatus): self
	{
		if (!$this->stationStatuses->contains($stationStatus)) {
			$this->stationStatuses[] = $stationStatus;
			$stationStatus->setVisit($this);
		}

		return $this;
	}

	public function removeStationStatus(StationStatus $stationStatus): self
	{
		if ($this->stationStatuses->removeElement($stationStatus)) {
			// set the owning side to null (unless already changed)
			if ($stationStatus->getVisit() === $this) {
				$stationStatus->setVisit(null);
			}
		}

		return $this;
	}

	public function getType(): ?VisitType
	{
		return $this->type;
	}

	public function setType(?VisitType $type): self
	{
		$this->type = $type;

		return $this;
	}

	public function getEventId(): ?string
	{
		return $this->eventId;
	}

	public function setEventId(string $eventId): self
	{
		$this->eventId = $eventId;

		return $this;
	}

	/**
	 * @return Collection|Station[]
	 */
	public function getInstalledStations(): Collection
	{
		return $this->installedStations;
	}

	public function addInstalledStation(Station $installedStation): self
	{
		if (!$this->installedStations->contains($installedStation)) {
			$this->installedStations[] = $installedStation;
			$installedStation->setInstalledDuringVisit($this);
		}

		return $this;
	}

	public function removeInstalledStation(Station $installedStation): self
	{
		if ($this->installedStations->removeElement($installedStation)) {
			// set the owning side to null (unless already changed)
			if ($installedStation->getInstalledDuringVisit() === $this) {
				$installedStation->setInstalledDuringVisit(null);
			}
		}

		return $this;
	}

	public function getStartingEliminationPeriod(): ?EliminationPeriod
	{
		return $this->startingEliminationPeriod;
	}

	public function setStartingEliminationPeriod(EliminationPeriod $startingEliminationPeriod): self
	{
		// set the owning side of the relation if necessary
		if ($startingEliminationPeriod->getStartVisit() !== $this) {
			$startingEliminationPeriod->setStartVisit($this);
		}

		$this->startingEliminationPeriod = $startingEliminationPeriod;

		return $this;
	}

	public function getEndingEliminationPeriod(): ?EliminationPeriod
	{
		return $this->endingEliminationPeriod;
	}

	public function setEndingEliminationPeriod(?EliminationPeriod $endingEliminationPeriod): self
	{
		// unset the owning side of the relation if necessary
		if ($endingEliminationPeriod === null && $this->endingEliminationPeriod !== null) {
			$this->endingEliminationPeriod->setEndVisit(null);
		}

		// set the owning side of the relation if necessary
		if ($endingEliminationPeriod !== null && $endingEliminationPeriod->getEndVisit() !== $this) {
			$endingEliminationPeriod->setEndVisit($this);
		}

		$this->endingEliminationPeriod = $endingEliminationPeriod;

		return $this;
	}

	public function getDiverseObservation(): ?string
	{
		return $this->diverseObservation;
	}

	public function setDiverseObservation(?string $diverseObservation): self
	{
		$this->diverseObservation = $diverseObservation;

		return $this;
	}

	public function getInternObservation(): ?string
	{
		return $this->internObservation;
	}

	public function setInternObservation(?string $internObservation): self
	{
		$this->internObservation = $internObservation;

		return $this;
	}

	public function getClientReportFileID(): ?string
	{
		return $this->clientReportFileID;
	}

	public function setClientReportFileID(?string $clientReportFileID): self
	{
		$this->clientReportFileID = $clientReportFileID;

		return $this;
	}

	public function getInternReportFileID(): ?string
	{
		return $this->InternReportFileID;
	}

	public function setInternReportFileID(?string $InternReportFileID): self
	{
		$this->InternReportFileID = $InternReportFileID;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getDurationAdjustments(): array
	{
		return $this->durationAdjustments;
	}

	/**
	 * @param array $durationAdjustments
	 * @return Visit
	 */
	public function setDurationAdjustments(array $durationAdjustments): Visit
	{
		$this->durationAdjustments = $durationAdjustments;
		return $this;
	}

	/**
	 * @return Collection|VisitStepTypeAdjustment[]
	 */
	public function getVisitStepTypeAdjustments(): Collection
	{
		return $this->visitStepTypeAdjustments;
	}

	public function addVisitStepTypeAdjustment(?VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if ($visitStepTypeAdjustment && !$this->visitStepTypeAdjustments->contains($visitStepTypeAdjustment)) {
			$this->visitStepTypeAdjustments[] = $visitStepTypeAdjustment;
			$visitStepTypeAdjustment->setAddedDuringVisit($this);
		}

		return $this;
	}

	public function removeVisitStepTypeAdjustment(VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if ($this->visitStepTypeAdjustments->removeElement($visitStepTypeAdjustment)) {
			// set the owning side to null (unless already changed)
			if ($visitStepTypeAdjustment->getAddedDuringVisit() === $this) {
				$visitStepTypeAdjustment->setAddedDuringVisit(null);
			}
		}

		return $this;
	}

	public function getNextScheduledVisit(): ?NextScheduledVisit
	{
		return $this->nextScheduledVisit;
	}

	public function setNextScheduledVisit(NextScheduledVisit $nextScheduledVisit): self
	{
		// set the owning side of the relation if necessary
		if ($nextScheduledVisit->getPreviousVisit() !== $this) {
			$nextScheduledVisit->setPreviousVisit($this);
		}

		$this->nextScheduledVisit = $nextScheduledVisit;

		return $this;
	}
}

