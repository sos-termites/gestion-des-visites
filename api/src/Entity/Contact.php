<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 */
#[ApiResource]
class Contact
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $name;

	/**
	 * @ORM\Column(type="string", length=13, unique=true)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $phone_number;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $email_address;

	/**
	 * @ORM\ManyToMany(targetEntity=Worksite::class, mappedBy="contacts")
	 */
	private $worksites;

	public function __construct()
	{
		$this->worksites = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getPhoneNumber(): ?string
	{
		return $this->phone_number;
	}

	public function setPhoneNumber(string $phone_number): self
	{
		$this->phone_number = $phone_number;

		return $this;
	}

	public function getEmailAddress(): ?string
	{
		return $this->email_address;
	}

	public function setEmailAddress(string $email_address): self
	{
		$this->email_address = $email_address;

		return $this;
	}

	/**
	 * @return Collection|Worksite[]
	 */
	public function getWorksites(): Collection
	{
		return $this->worksites;
	}

	public function addWorksite(Worksite $worksite): self
	{
		if (!$this->worksites->contains($worksite)) {
			$this->worksites[] = $worksite;
			$worksite->addContact($this);
		}

		return $this;
	}

	public function removeWorksite(Worksite $worksite): self
	{
		if ($this->worksites->removeElement($worksite)) {
			$worksite->removeContact($this);
		}

		return $this;
	}
}
