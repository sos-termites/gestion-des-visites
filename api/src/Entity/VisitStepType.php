<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Filter\VisitStepTypeFilter;
use App\Repository\VisitStepTypeRepository;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=VisitStepTypeRepository::class)
 */
#[ApiResource(
	collectionOperations: [
		'get' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'post' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]]],
	itemOperations: [
		'get' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'put' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'delete' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'patch' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]]],
	normalizationContext: ['groups' => ['visit_type']],
	security: 'is_granted("ROLE_USER")')]
#[ApiFilter(VisitStepTypeFilter::class)]
class VisitStepType
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	private $short_name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	private $long_name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $code;

	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	private ?DateInterval $duration = null;

	/**
	 * @ORM\OneToMany(targetEntity=VisitStepTypeAdjustment::class, mappedBy="visitStepType", orphanRemoval=true)
	 */
	#[Groups(['visit_type'])]
	private $adjustments;

	/**
	 * @ORM\Column(type="boolean", nullable="no")
	 */
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	private $isDurationUnit;

	public function __construct()
	{
		$this->setDuration(DateInterval::createFromDateString('0 min'));
		$this->adjustments = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getShortName(): ?string
	{
		return $this->short_name;
	}

	public function setShortName(string $short_name): self
	{
		$this->short_name = $short_name;

		return $this;
	}

	public function getLongName(): ?string
	{
		return $this->long_name;
	}

	public function setLongName(string $long_name): self
	{
		$this->long_name = $long_name;

		return $this;
	}

	public function getCode(): ?string
	{
		return $this->code;
	}

	public function setCode(string $code): self
	{
		$this->code = $code;

		return $this;
	}

	/**
	 * @return DateInterval|null
	 */
	public function getDuration(): ?DateInterval
	{
		return $this->duration;
	}

	/**
	 * @param DateInterval $duration
	 * @return VisitStepType
	 */
	public function setDuration(DateInterval $duration): self
	{
		$this->duration = $duration;
		return $this;
	}

	/**
	 * @return Collection|VisitStepTypeAdjustment[]
	 */
	public function getAdjustments(): Collection
	{
		return $this->adjustments;
	}

	public function addAdjustment(VisitStepTypeAdjustment $adjustment): self
	{
		if (!$this->adjustments->contains($adjustment)) {
			$this->adjustments[] = $adjustment;
			$adjustment->setVisitStepType($this);
		}

		return $this;
	}

	public function removeAdjustment(VisitStepTypeAdjustment $adjustment): self
	{
		if ($this->adjustments->removeElement($adjustment)) {
			// set the owning side to null (unless already changed)
			if ($adjustment->getVisitStepType() === $this) {
				$adjustment->setVisitStepType(null);
			}
		}

		return $this;
	}

	public function removeAllAdjustments(): self
	{
		$this->adjustments = new ArrayCollection();
		return $this;
	}

	public function getIsDurationUnit(): ?bool
	{
		return $this->isDurationUnit;
	}

	public function setIsDurationUnit(bool $isDurationUnit): self
	{
		$this->isDurationUnit = $isDurationUnit;

		return $this;
	}
}
