<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class AddressTest extends ApiTestCase
{
	public function testCreateAddress()
	{
		static::createClient()->request('POST', '/addresses', ['json' => [
			'street' => '800 Avenue du Parc des Expositions',
			'postalCode' => 33260,
			"city" => "LA TESTE DE BUCH"
		]]);

		$this->assertResponseStatusCodeSame(201);
		$this->assertJsonContains([
			'@context' => '/contexts/Address',
			'@type' => 'Address',
			'street' => '800 Avenue du Parc des Expositions',
			'postal_code' => 33260,
			"city" => "LA TESTE DE BUCH"
		]);
	}
}
