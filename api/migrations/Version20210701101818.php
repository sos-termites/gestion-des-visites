<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210701101818 extends AbstractMigration
{
	public function getDescription(): string
	{
		return '';
	}

	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('DROP SEQUENCE greeting_id_seq CASCADE');
		$this->addSql('DROP TABLE greeting');
		$this->addSql('ALTER TABLE worksite ADD installation_date DATE NOT NULL');
		$this->addSql('ALTER TABLE worksite ADD next_visit_limit_date DATE NOT NULL');
		$this->addSql('ALTER TABLE worksite ADD protected_built_surface INT NOT NULL');
		$this->addSql('ALTER TABLE worksite ADD protected_built_perimeter INT NOT NULL');
		$this->addSql('ALTER TABLE worksite ADD protected_undeveloped_surface INT NOT NULL');
		$this->addSql('ALTER TABLE worksite ADD protected_indeveloped_perimeter INT NOT NULL');
	}

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SCHEMA public');
		$this->addSql('CREATE SEQUENCE greeting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
		$this->addSql('CREATE TABLE greeting (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
		$this->addSql('ALTER TABLE worksite DROP installation_date');
		$this->addSql('ALTER TABLE worksite DROP next_visit_limit_date');
		$this->addSql('ALTER TABLE worksite DROP protected_built_surface');
		$this->addSql('ALTER TABLE worksite DROP protected_built_perimeter');
		$this->addSql('ALTER TABLE worksite DROP protected_undeveloped_surface');
		$this->addSql('ALTER TABLE worksite DROP protected_indeveloped_perimeter');
	}
}
