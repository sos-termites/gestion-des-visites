<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630115310 extends AbstractMigration
{
	public function getDescription(): string
	{
		return '';
	}

	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SEQUENCE visit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
		$this->addSql('CREATE TABLE visit (id INT NOT NULL, worksite_id INT NOT NULL, scheduled_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, estimated_duration VARCHAR(255) NOT NULL, effective_duration VARCHAR(255) NOT NULL, is_pending BOOLEAN NOT NULL, is_completed BOOLEAN NOT NULL, PRIMARY KEY(id))');
		$this->addSql('CREATE INDEX IDX_437EE939A47737E7 ON visit (worksite_id)');
		$this->addSql('COMMENT ON COLUMN visit.estimated_duration IS \'(DC2Type:dateinterval)\'');
		$this->addSql('COMMENT ON COLUMN visit.effective_duration IS \'(DC2Type:dateinterval)\'');
		$this->addSql('ALTER TABLE visit ADD CONSTRAINT FK_437EE939A47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
		$this->addSql('ALTER TABLE station DROP CONSTRAINT fk_9f39f8b1a47737e7');
		$this->addSql('DROP INDEX idx_9f39f8b1a47737e7');
		$this->addSql('ALTER TABLE station DROP worksite_id');
	}

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SCHEMA public');
		$this->addSql('DROP SEQUENCE visit_id_seq CASCADE');
		$this->addSql('DROP TABLE visit');
		$this->addSql('ALTER TABLE station ADD worksite_id INT NOT NULL');
		$this->addSql('ALTER TABLE station ADD CONSTRAINT fk_9f39f8b1a47737e7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
		$this->addSql('CREATE INDEX idx_9f39f8b1a47737e7 ON station (worksite_id)');
	}
}
